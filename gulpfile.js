const babel = require('gulp-babel');
const browserify = require('browserify');
const concat = require('gulp-concat');
const connect = require('gulp-connect');
const gulp = require('gulp');
const livereload = require('gulp-livereload');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const watchify = require('watchify');

const babelOptions = {
  plugins: ['@babel/plugin-proposal-function-bind', '@babel/plugin-proposal-class-properties'],
  presets: ['@babel/env', '@babel/react'],
};

function onError(err) {
  console.log('[Compilation Error]');
  console.log(err.fileName + ( err.loc ? `( ${err.loc.line}, ${err.loc.column} ): ` : ': '));
  console.log('error Babel: ' + err.message + '\n');

  if (err.codeFrame) {
    console.log(err.codeFrame);
  }

  this.emit('end');
  process.exit(1);
}

gulp.task('server', () => {
  connect.server({
    host: '0.0.0.0',
    root: ['example', 'build', 'styles'],
    port: 8001,
    livereload: true,
  });
});

gulp.task('sass', (cb) => {
  gulp.src('./styles/scss/image-gallery.scss')
    .pipe(sass())
    .on('error', onError)
    .pipe(rename('image-gallery.css'))
    .pipe(gulp.dest('./styles/css/'))
    .pipe(livereload());
  cb();
});

gulp.task('scripts', () => {
  watchify(browserify({
    entries: './example/app.js',
    extensions: ['.jsx'],
    debug: true,
  }).transform('babelify', babelOptions))
    .bundle()
    .on('error', (err) => console.error('error is', err))
    .pipe(source('example.js'))
    .pipe(buffer())
    .pipe(gulp.dest('./example/'))
    .pipe(livereload());
});

gulp.task('demo-src', () => {
  process.env.NODE_ENV = 'production';
  browserify({
    entries: './example/app.js',
    extensions: ['.jsx'],
    debug: true,
  }).transform('babelify', babelOptions)
    .bundle()
    .pipe(source('demo.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('./demo/'));

  gulp.src(['./styles/css/image-gallery.css', './example/app.css'])
    .pipe(concat('demo.css'))
    .pipe(cleanCSS({ keepSpecialComments: false }))
    .pipe(gulp.dest('./demo/'));
});

gulp.task('source-js', (cb) => {
  gulp.src('./src/ImageGallery.jsx')
    .pipe(concat('image-gallery.js'))
    .pipe(babel(babelOptions))
    .on('error', onError)
    .pipe(gulp.dest('./build'));
  cb();
});

gulp.task('svg-js', (cb) => {
  gulp.src('./src/SVG.jsx')
    .pipe(concat('SVG.js'))
    .pipe(babel(babelOptions))
    .on('error', onError)
    .pipe(gulp.dest('./build'));
  cb();
});

gulp.task('watch', () => {
  livereload.listen();
  gulp.watch(['styles/**/*.scss'], ['sass']);
  gulp.watch(['src/*.jsx', 'src/icons/*.jsx', 'example/app.js'], ['scripts']);
});

//gulp.task('dev', ['watch', 'scripts', 'sass', 'server']);
gulp.task('build', gulp.series('source-js', 'svg-js', 'sass'));
//gulp.task('demo', ['demo-src']);
